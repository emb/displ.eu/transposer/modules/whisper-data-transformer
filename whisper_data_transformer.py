import spacy

class JsonTransformer:
    def __init__(self, data):
        self.data = data
        if "language" not in data:
            print('Missing text language')
            self.lang = data["language"]

    def sentence_segmentation(self):
        if "language" in self.data and "text" in self.data:
            self.sentence_segmentation(self.data["text"], self.data["language"])
        else:
            print('Missing text language or plain text')

    # Split full-text into list of sentences
    def sentence_segmentation(self, text: str, lang: str):
        # List of punctuation marks to split sentences
        punctuation_marks = ['.', '?', '!', '।', '،', '。', '؟', '¡', '¿', ':', ';', '...', '·']
        # dictionary to Spacy language model
        spacy_model = {
            'en': 'en_core_web_sm',
            'de': 'de_core_news_sm',
            'ca': 'ca_core_news_sm',
            'zh': 'zh_core_web_sm',
            'hr': 'hr_core_news_sm',
            'da': 'da_core_news_sm',
            'nl': 'nl_core_news_sm',
            'fi': 'fi_core_news_sm',
            'fr': 'fr_core_news_sm',
            'el': 'el_core_news_sm',
            'it': 'it_core_news_sm',
            'ja': 'ja_core_news_sm',
            'ko': 'ko_core_news_sm',
            'lt': 'lt_core_news_sm',
            'mk': 'mk_core_news_sm',
            'nb': 'nb_core_news_sm',
            'pl': 'pl_core_news_sm',
            'pt': 'pt_core_news_sm',
            'ro': 'ro_core_news_sm',
            'ru': 'ru_core_news_sm',
            'sl': 'sl_core_news_sm',
            'es': 'es_core_news_sm',
            'sv': 'sv_core_news_sm',
            'uk': 'uk_core_news_sm',
            'xx': 'xx_ent_wiki_sm',
        }
        # Intitate sentences list
        sentence_list = []

        if lang in spacy_model:
            # Load the Spacy language model
            nlp = spacy.load(spacy_model[lang])
            # Remove unnescessary spaces
            doc = nlp(text.strip())
            # Iterate through sentences
            for sentence in doc.sents:
                sentence_list.append(str(sentence))
        else:
            sentence = ''
            for char in text:
                if char in punctuation_marks:
                    sentence = sentence + char
                    sentence_list.append(sentence)
                    sentence = ''
                else:
                    sentence = sentence + char

        checked_sentence_list = []

        new_sentence = ''
        for sentence in sentence_list:
            # Check if the string is not empty
            if sentence:
                last_char = sentence[-1]
                if last_char not in punctuation_marks:
                    new_sentence += " " + sentence
                    # checked_sentence_list.append(sentence)
                else:
                    if new_sentence == "":
                        checked_sentence_list.append(sentence)
                    else:
                        new_sentence += " " + sentence
                        checked_sentence_list.append(new_sentence.strip())
                        new_sentence = ''
        return checked_sentence_list

    # Transform segments part to sentence to sentence
    def json_transform(self):
        sentences_obj = []
        index = 0
        words_list = []
        sentence_id = 0
        # Get sentences list
        sentence_list = self.sentence_segmentation(self.data['text'], self.data['language'])
        for sentence in sentence_list:
            # Get last word of each sentence
            last_word = sentence.split()[-1]
            # Iterate through segments
            for segment in self.data['segments']:
                temp = False
                words = segment['words']
                clone_words = words.copy()
                # Iterate through word timestamp
                for word in clone_words:
                    # Get word
                    word_text = word['text']
                    # If word is the last word of sentence
                    if last_word == word_text:
                        words_list.append(word)
                        # Remove word fromm segment to prevent duplication/ save time
                        words.pop(0)
                        # New segment to store sentence
                        temp_segment = {}
                        # Get timestamp of whole segment
                        if len(words_list) == 1:
                            temp_segment = {
                                "id": sentence_id,
                                "start": words_list[0]['start'],
                                "end": words_list[0]['end'],
                                "text":  sentence_list[index]
                            }
                        else:
                            temp_segment = {
                                "id": sentence_id,
                                "start": words_list[0]['start'],
                                "end": words_list[len(words_list)-1]['end'],
                                "text": sentence_list[index]
                            }
                        # Add completed sentence segment
                        sentences_obj.append(temp_segment)
                        sentence_id += 1
                        # Reset array to store new sentence's words
                        words_list = []
                        temp = True
                        break
                    else:
                        # Add wordd with timestamp to array
                        words_list.append(word)
                        words.pop(0) 
                # Break for segment loop
                if temp:
                    break
            index += 1

        # Create a duplicate dictionary
        self.transform_data = self.data.copy()
        self.transform_data['segments'] = sentences_obj

        return self.transform_data['segments']
    
    # Perform translation
    def translate(self):
        text = self.data['text']
        translated_text = "Translation magic here"
        return translated_text
    
    def get_result(self):
            # Get text for translation
            total = self.transform_data['text']
            # Perform translation
            translated_text = self.translate()
            # Split translated text into sentence
            # translated_list = self.sentence_segmentation(self, translated_text, 'en')
            # Get transfomed object
            translated_data = self.transform_data
            # Get translation back to segments
            for i, segment in enumerate(translated_data['segments']):
                segment['text'] = translated_text[i]
            # Get full-text back to object
            translated_data['text'] = translated_text
            return translated_data
